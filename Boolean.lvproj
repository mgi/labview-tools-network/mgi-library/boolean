﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="boolean" Type="Folder">
			<Item Name="Boolean Debounce.vi" Type="VI" URL="../Boolean Debounce.vi"/>
			<Item Name="MGI Boolean.vipb" Type="Document" URL="../MGI Boolean.vipb"/>
			<Item Name="MGI Boolean.vipc" Type="Document" URL="../MGI Boolean.vipc"/>
			<Item Name="Resettable Trigger (Reentrant).vi" Type="VI" URL="../Resettable Trigger (Reentrant).vi"/>
			<Item Name="Resettable Trigger.vi" Type="VI" URL="../Resettable Trigger.vi"/>
			<Item Name="Symmetric Boolean Debounce.vi" Type="VI" URL="../Symmetric Boolean Debounce.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
